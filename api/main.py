from typing import Any
from firebase_admin import messaging, credentials
import firebase_admin
import json

class FcmUtils:
    def __init__(self):
        creds = credentials.Certificate(
            'shift-alerts-firebase-adminsdk-u6xvb-1541b8c07a.json')
        default_app = firebase_admin.initialize_app(creds)

    # send_to_token
    # Send a message to a specific token
    # registration_token: The token to send the message to
    # data: The data to send to the token
    # {
    #   'score': '850',
    #   'time': '2:45',
    # },
    # example
   
    def send_to_token(self, registration_token, title, body, data,content) :
        data1={
                'payload': {
                    'peerId': '3',
                    'peerName': 'Rohan Kumar',
                    'channelId': "12_3",
                    'message': "hello",
                    'message_type': "msg",
                    'image': 'static'
                    },
                    'content': {
                    'channelKey': 'shift_alerts',
                    'title': 'Rohan Kumar',
                    'body': "hello",
                    'largeIcon': '',
                    'bigPicture': '',
                    }
            },
        message = messaging.Message(
            notification=messaging.Notification(
                title=title,
                body=body,
            ),
            
            data={
                    'payload': json.dumps(data),
                    'content': json.dumps(content)
            },
                    
        
            token=registration_token
        )
        response = messaging.send(message)
        print(response)
        return response

    # send_to_token_multicast
    # Send a message to a specific tokens
    # registration_tokens: The tokens to send the message to
    # data: The data to send to the tokens
    def send_to_token_multicast(self, registration_tokens, title, body, data=None) -> Any:
        # registration_tokens has to be a list
        assert isinstance(registration_tokens, list)

        message = messaging.MulticastMessage(
            notification=messaging.Notification(
                title=title,
                body=body,
            ),
            data=data,
            token=registration_tokens
        )
        response = messaging.send_multicast(message)
        print(response)
        # See the BatchResponse reference documentation
        # for the contents of response.
        return response

    # send_to_topic
    # Send a message to a topic
    # topic: The topic to send the message to
    # data: The data to send to the topic
    # {
    #   'score': '850',
    #   'time': '2:45',
    # },
    # example
    def send_to_topic(self, topic, title, body, data=None) -> Any:
        message = messaging.Message(
            notification=messaging.Notification(
                title=title,
                body=body,
            ),
            data=data,
            topic=topic
        )
        response = messaging.send(message)
        print(response)
        # Response is a message ID string.
        return response

    # def all_platforms_message():
    #         # [START multi_platforms_message]
    #         message = messaging.Message(
    #             notification=messaging.Notification(
    #                 title='$GOOG up 1.43% on the day',
    #                 body='$GOOG gained 11.80 points to close at 835.67, up 1.43% on the day.',
    #             ),
    #             android=messaging.AndroidConfig(
    #                 ttl=datetime.timedelta(seconds=3600),
    #                 priority='normal',
    #                 notification=messaging.AndroidNotification(
    #                     icon='stock_ticker_update',
    #                     color='#f45342'
    #                 ),
    #             ),
    #             apns=messaging.APNSConfig(
    #                 payload=messaging.APNSPayload(
    #                     aps=messaging.Aps(badge=42),
    #                 ),
    #             ),
    #             topic='industry-tech',
    #         )
    # # [END multi_platforms_message]
    #         return message