
from xml.dom import ValidationErr
from rest_framework import serializers
from healthcare.models import *
from django.utils.encoding import smart_str, force_bytes, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode,urlsafe_base64_encode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
import bcrypt

class MyUserSerializer(serializers.ModelSerializer):

    # password2=serializers.CharField(style={'input_type':'password'},write_only=True)
    class Meta:
        model=MyUser
        fields=['email','username','position','password','id']
        extra_kwargs={
            'password':{'write_only':True}
        }

    def create(self,validate_data):
        return MyUser.objects.create_user(**validate_data)


class MyUserLoginSerializer(serializers.ModelSerializer):
    email=serializers.EmailField(max_length=255)
    class Meta:
        model=MyUser
        fields=['email','password']

class MyUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model=MyUser
        fields=['id','username','email','position','password','user_id','Admin']

class MyUserPasswordChangeSerializer(serializers.ModelSerializer):
    old_password=serializers.CharField(max_length=255, style={'input_type':'password'},write_only=True)
    password=serializers.CharField(max_length=255, style={'input_type':'password'},write_only=True)
    password2=serializers.CharField(max_length=255, style={'input_type':'password'},write_only=True)
    class Meta:
        model=MyUser
        fields=['old_password','password','password2']
    
    def validate(self,attrs):
        import base64
        old_password=base64.b64decode(attrs.get('old_password'))
        password=base64.b64decode(attrs.get('password'))
        password2=base64.b64decode(attrs.get('password2'))
      
        old_password=old_password.decode('utf-8')
        password=password.decode('utf-8')
        password2=password2.decode('utf-8')
        user=self.context.get('user')
      
        if (user.check_password(old_password)):
            if password != password2:
                raise serializers.ValidationError('passwords not matched')
            user.set_password(password)
            user.save()
            if(user.user_id!=''):
                Clinician.objects.filter(clinician_id=user.user_id).update(password=password)
            elif(user.Admin!=''):
                Admin.objects.filter(admin_id=user.Admin).update(password=password)
            return attrs
        else:
            raise serializers.ValidationError('Enter your correct current password')
      
            
       


class MyUserUpdateSerializer(serializers.ModelSerializer):
    email=serializers.EmailField(max_length=255)
    username=serializers.CharField(max_length=50)
    position=serializers.CharField(max_length=14)
    user_id=serializers.CharField(max_length=50)
    class Meta:
        model=MyUser
        fields=['email','username','position']
    
    
    def validate(self,attrs):
        email=attrs.get('email')
        username=attrs.get('username')
        position=attrs.get('position')
        user=self.context.get('user')
        user.email=email
        user.username=username
        user.save()
        
        return attrs

from .utils import *
from email.mime.text import MIMEText
class SendPasswordSerializer(serializers.ModelSerializer):
    email=serializers.CharField(max_length=255)
    class Meta:
        model=MyUser
        fields=['email']
    def validate(self,attrs):

        from email.mime.text import MIMEText

        email=attrs.get('email')
        user=MyUser.objects.get(email=email)
        uid=urlsafe_base64_encode(force_bytes(user.id))
        token=PasswordResetTokenGenerator().make_token(user)
        link='http://44.203.161.90/change/'+uid+'/'+token
        #Sending Email
        body='Click following link to reset your password  '+link
        data={
            'subject':"Reset Password Link",
            'body':body,
            'to_email':user.email
        }
        Util.send_email(data)
        return attrs
        
        


class UserPasswordReset(serializers.ModelSerializer):
  
    password=serializers.CharField(max_length=255, style={'input_type':'password'},write_only=True)
    password2=serializers.CharField(max_length=255, style={'input_type':'password'},write_only=True)
    class Meta:
        model=MyUser
        fields=['password','password2']
   
    def validate(self,attrs):
        try:
            password=attrs.get('password')
            password2=attrs.get('password2')
            
            uid=self.context.get('uid')
            token=self.context.get('token')
            if password != password2:
                pass
            id=smart_str(urlsafe_base64_decode(uid))
            user=MyUser.objects.get(id=id)
            if not PasswordResetTokenGenerator().check_token(user,token):
                pass
            user.set_password(password)
            user.save()
            if(user.position=='c_level_emp' or user.position=='hmc_admin' or user.position=='facility_admin'):
                Admin.objects.filter(email=user.email).update(password=user.password)
            return attrs
        
        except DjangoUnicodeDecodeError as identifier:
            PasswordResetTokenGenerator().check_token(user,token)
            raise serializers.ValidationError('token is expired or invalid')

             

class FacilitySerializer(serializers.ModelSerializer):
    
    class Meta:
        model=Facility
        fields='__all__'


class ClinicianSerializer(serializers.ModelSerializer):
    class Meta:
        model=Clinician
        fields='__all__'
    def create(self,validate_data):
        from django.contrib.auth.hashers import make_password
        validate_data['password']=make_password(validate_data['password'])
        return super(ClinicianSerializer,self).create(validate_data)
    

class AdminSerializer(serializers.ModelSerializer):
    class Meta:
        model=Admin
        fields='__all__'
class AdminPasswodSerializer(serializers.ModelSerializer):
    class Meta:
        model=Admin
        fields='__all__'


class MyUserViewSerializer(serializers.ModelSerializer):
    class Meta:
        model=MyUser
        exclude=('password',)


class SchedulingSerializer(serializers.ModelSerializer):
    start_date=serializers.DateField()
    end_date=serializers.DateField()
    class Meta:
        model = Scheduling
        fields = '__all__'
    
   

class AddshiftSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Shift_template
        fields = '__all__'
     


class TimesheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timesheet
        fields = '__all__'


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        fields = '__all__'
class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feedback
        fields = '__all__'


class Daily_QuotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Daily_Quotes
        fields = '__all__'


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'

class NotesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = '__all__'

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = '__all__'
        
class ChatHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatHistory
        fields = '__all__'


class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = '__all__'

class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'

class ChatNotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChatNotification
        # fields = '__all__'
        exclude=('chat_notification_id','platform','userId',)

