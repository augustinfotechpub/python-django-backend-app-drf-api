from cgitb import lookup
from datetime import date
from logging import raiseExceptions
from multiprocessing import context
from sys import api_version
from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from healthcare.models import *
from .serializers import *
from rest_framework import generics
from rest_framework.filters import SearchFilter
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from api.pagination import CustomPagination, DefaultCustomPagination
from django.contrib.auth import authenticate,login
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import redirect
from django.contrib.auth.hashers import make_password
import base64
import re
from datetime import datetime,timedelta
from io import BytesIO
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
import uuid
from django.conf import settings
import base64
import os
from dotenv import load_dotenv
import random
import requests
import json
from django.views.decorators.csrf import csrf_exempt
from django_filters.rest_framework import DjangoFilterBackend
from django.contrib.auth.hashers import make_password
from itertools import chain
import math,time
from agora_token_builder import RtcTokenBuilder
import boto3
from botocore.client import Config
from rest_framework import status
from .main import FcmUtils
load_dotenv()
messaging = FcmUtils()

# Create your views here.

#####################################################################
###### Function to generate Tokens ##################################
#####################################################################
def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


#####################################################################
###### Function to Register User ####################################
#####################################################################
class RegisterUserView(APIView):
    def post(self,request, format=None):
        serializer=MyUserSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user=serializer.save()
            token=get_tokens_for_user(user)
            return Response({'msg':'success','token':token})
        return Response(serializer.error)


#####################################################################
###### To Check Recaptcha Score #####################################
#####################################################################
class scoreView(APIView):
    def post(self,request, format=None):
            token=request.data['recaptchaToken']
            url='https://www.google.com/recaptcha/api/siteverify'
            headers={'Content-Type':'application/x-www-form-urlencoded'}
            data={"secret":"6LdtKOkhAAAAAF4egCutJY8ZO78eDG0FmoDcdLNo","response":token}
            x=requests.post(url,data=data,headers=headers)
            recaptcha=json.loads(x.content)
            return Response({'status':200,'payload':x.text})


#####################################################################
###### To Login for web #############################################
#####################################################################
class LoginUserWebView(APIView):
    def post(self,request, format=None):
            email=request.data['email']
            password=request.data['password']
            token=request.data['recaptchaToken']
            pat = "^[a-zA-Z0-9-_]+@[a-zA-Z0-9]+\.[a-z]{1,3}$"
            url='https://www.google.com/recaptcha/api/siteverify'
            headers={'Content-Type':'application/x-www-form-urlencoded'}
            secretKey=os.getenv('RECAPTCHA_SECRET_KEY')
            data={"secret":secretKey,"response":token}
            x=requests.post(url,data=data,headers=headers)
            recaptcha=json.loads(x.content)
            if recaptcha.get('success')==True:
                if re.match(pat,email):
                    email=email
                else:
                    try:
                        fetched_email=MyUser.objects.get(username=email).email     
                    
                        email=fetched_email
                    except:
                        return Response({'status':401,'payload':'login failed'})
            
                password=base64.b64decode(password)
                user=authenticate(email=email,password=password)
                if user is not None and user.position=='clinician':
                    return Response({'status':203,'payload':'Not Authorized'})
                elif user is not None and user.position!='clinician':
                    # login(request,user)
                    token=get_tokens_for_user(user)
                    return Response({'msg':'login success','token':token,'data':{'email':email,'password':password,'admin_id':str(user.Admin),"user_id":user.user_id,'position':user.position,'recaptcha':recaptcha}})
                else:
                    return Response({'status':401,'payload':'login failed'})
            else:
                return Response({'status':200,'payload':'login failed'})

           
#####################################################################
###### To Login for Flutter ##################################
##################################################################### 
class LoginUserView(APIView):
    def post(self,request, format=None):
            email=request.data['email']
            password=request.data['password']
            pat = "^[a-zA-Z0-9-_]+@[a-zA-Z0-9]+\.[a-z]{1,3}$"
            if re.match(pat,email):
                email=email
            else:
                try:
                    fetched_email=MyUser.objects.get(username=email).email     
                   
                    email=fetched_email
                except:
                    return Response({'status':401,'payload':'login failed'})
        
            password=base64.b64decode(password)
            user=authenticate(email=email,password=password)
           
            if user is not None:
                # login(request,user)
                token=get_tokens_for_user(user)
                return Response({'msg':'login success','token':token,'data':{'email':email,'password':password,'admin_id':str(user.Admin),"user_id":user.user_id,'position':user.position}})
            else:
                return Response({'status':401,'payload':'login failed'})
       
        
#####################################################################
###### To Display user details ######################################
#####################################################################
class UserProfileView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request, format=None):
        serializer=MyUserProfileSerializer(request.user)
        return Response({'msg':serializer.data})
        
        
# class UserPasswordChangeView(APIView):
#     permission_classes=[IsAuthenticated]
#     def post(self,request, format=None):
#         serializer=MyUserPasswordChangeSerializer(data=request.data,context={'user':request.user})
#         if serializer.is_valid(raise_exception=True):
#             return Response({'msg':'password changed'})
#         return Response({'msg':serializer.errors})

#####################################################################
###### To change user password ######################################
#####################################################################
class UserPasswordChangeView(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
        serializer=MyUserPasswordChangeSerializer(data=request.data,context={'user':request.user})
        if serializer.is_valid(raise_exception=True):
            return Response({'msg':'password changed'})
        return Response({'msg':serializer.errors})


#####################################################################
###### To Change user profile #######################################
#####################################################################
class UserPasswordChangeView1(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
        serializer=MyUserUpdateSerializer(data=request.data,context={'user':request.user})
        if serializer.is_valid(raise_exception=True):        
            return Response({'msg':'profile changed'})
        return Response({'msg':serializer.errors})

    def patch(self,request,id):
        try:
            user_obj=MyUser.objects.get(id=request.user.id)
            serializer=MyUserViewSerializer(user_obj,data=request.data,partial=True)
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    user=serializer.save()
                    return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':serializer.errors})


#####################################################################
###### To Change user profile #### ##################################
#####################################################################
class UserProfileUpdate(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        user=MyUser.objects.filter(pk=id).update(username=request.data['username'],position=request.data['position'],email=request.data['email'],password=make_password(request.data['password']))
        return Response({'payload':user})
       


#####################################################################
###### To send email to user ########################################
#####################################################################
class SendEmailPassword(APIView):
    def post(self,request,format=None):
        serializer=SendPasswordSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            return Response({'msg':'password reset link sent. Check your email'})
        else:
            return Response({'msg':serializer.errors})


#####################################################################
###### To Reset user password #######################################
#####################################################################
class UserPasswordResetView(APIView):
     def post(self,request,uid,token,format=None):
        serializer=UserPasswordReset(data=request.data,context={'uid':uid,'token':token})
        if serializer.is_valid(raise_exception=True):
            return Response({'msg':'password reset successfully'})
        else:
            return Response({'msg':serializer.errors})



#####################################################################
###### To Change user password ######################################
#####################################################################
@csrf_exempt
def ChangePassword(request,uid,token):
    if request.method=='POST':
        password=request.POST['password']
        password2=request.POST['password2']
        if(password!=password2):
            return render(request,'change-password.html',{'msg':'Passwords not matched'})
        serializer=UserPasswordReset(data={"password":password,'password2':password2},context={'uid':uid,'token':token})
        if serializer.is_valid(raise_exception=True):
            # return Response({'msg':'password reset successfully'})
            return render(request,'success.html')
        else:
            return Response({'msg':serializer.errors})
    return render(request,'change-password.html')



#####################################################################
###### To Display user profile ######################################
#####################################################################
class MyUserView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = MyUser.objects.all()
    serializer_class = MyUserSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["^email"]
    filterset_fields=['user_id']

#####################################################################
###### To Display and create facilities #############################
#####################################################################
class FacilityView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Facility.objects.all().order_by('facility_id')
    serializer_class = FacilitySerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["^facility_name","^address","facility_id","is_active"]
    filterset_fields=['facility_id']
    
#####################################################################
###### To update and delete facilities ##############################
#####################################################################
class FacilityUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Facility.objects.all()
    serializer_class = FacilitySerializer
    lookup_field= 'facility_id'



#####################################################################
###### To Display and create clinicians #############################
#####################################################################
class ClinicianView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Clinician.objects.all().order_by('clinician_id')
    serializer_class = ClinicianSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["=clinician_position","^email","^firstname","^lastname","^contact_no","^job_sites","clinician_id","is_active"]
    filterset_fields=['clinician_id','is_active','clinician_position']
    pagination_class=DefaultCustomPagination

#####################################################################
###### To display and create admins #################################
#####################################################################
class AdminView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Admin.objects.all()
    serializer_class = AdminSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["facility__facility_name","role","=admin_id","is_active","firstname","lastname","contact_no","email"]
    filterset_fields=['admin_id','facility','role']
    pagination_class=DefaultCustomPagination

#####################################################################
###### To Display admins according to filter ########################
#####################################################################
class OnlyAdminView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Admin.objects.all().filter(role__in=('hmc_admin','c_level_emp'))
    serializer_class = AdminSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["facility__facility_name","^role","=admin_id","is_active","firstname","lastname","contact_no","email"]
    filterset_fields=['admin_id','facility','role']
    pagination_class=DefaultCustomPagination

#####################################################################
###### To update and delete clinicians ##############################
#####################################################################
class AdminUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Admin.objects.all()
    serializer_class = AdminSerializer
    lookup_field= 'admin_id'



#####################################################################
###### To Display and create schedules #############################
#####################################################################
class SchedulingView(generics.ListAPIView,generics.CreateAPIView):   
    permission_classes=[IsAuthenticated]
    queryset = Scheduling.objects.all().order_by('start_date')
    serializer_class = SchedulingSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["start_date","facility_id","status","clinician_position"]
    filterset_fields=['facility_id','clinician_position','clinician_id']
   
   
#####################################################################
###### To Display,create,update,delete shifts #######################
#####################################################################
class calenderviews(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        year=self.request.GET.get('year')
        month=self.request.GET.get('month')
        day=self.request.GET.get('day')
        
        if (year):
            admin_obj=Scheduling.objects.filter(start_date__year=year)
            serializer=SchedulingSerializer(admin_obj,many=True)
            return Response({'status':200,'payload':serializer.data})
        elif (month):
            admin_obj=Scheduling.objects.filter(start_date__month=month)
            serializer=SchedulingSerializer(admin_obj,many=True)
            return Response({'status':200,'payload':serializer.data})
        elif (day):
            admin_obj=Scheduling.objects.filter(start_date__day=day)
            serializer=SchedulingSerializer(admin_obj,many=True)
            return Response({'status':200,'payload':serializer.data})
        else:
            return Response({'status':200,'payload':"no data"})


    def post(self,request):
        serializer=SchedulingSerializer(data=request.data)
        if not serializer.is_valid():
            return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
        serializer.save()
        return Response({'status':200,'payload':serializer.data})

        
    def put(self,request,id):
        try:
            admin_obj=Scheduling.objects.get(admin_id=id)
            serializer=SchedulingSerializer(admin_obj,data=request.data)
            if not serializer.is_valid():
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            serializer.save()
            return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})
    def patch(self,request,id):
        try:
            admin_obj=Scheduling.objects.get(admin_id=id)
            serializer=SchedulingSerializer(admin_obj,data=request.data,partial=True)
            if not serializer.is_valid():
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})
    def delete(self,request,id):
        try:
            admin_obj=Scheduling.objects.get(admin_id=id)
            admin_obj.delete()
            return Response({'status':200,'message':'deleted'})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})
        

#####################################################################
###### To update and delete shifts ##################################
#####################################################################
class SchedulingUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Scheduling.objects.all()
    serializer_class = SchedulingSerializer
    lookup_field= 'schedule_id'
    


#####################################################################
###### To Display and create shifts templates #######################
#####################################################################
class Shift_View(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Shift_template.objects.all()
    serializer_class = AddshiftSerializer


#####################################################################
###### To update and delete shifts templates ########################
#####################################################################
class Shift_Update(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Shift_template.objects.all()
    serializer_class = AddshiftSerializer
    lookup_field = 'shift_template_id'


#####################################################################
###### To Display and create timesheets #############################
#####################################################################
class TimesheetView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Timesheet.objects.all().order_by('timesheet_id')
    serializer_class = TimesheetSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=['timesheet_id','facility_note','clinician_note','clinician_position','^clinician_name','^facility_name']
    filterset_fields=['clinician_id','clinician_position','facility_id']
    pagination_class=DefaultCustomPagination


#####################################################################
###### To update and delete timesheets ##############################
#####################################################################
class TimesheetUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Timesheet.objects.all()
    serializer_class = TimesheetSerializer
    lookup_field= 'timesheet_id'


#####################################################################
###### To Display,create,update,delete facilities ###################
#####################################################################
class FacilityAPI(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        facility_obj=Facility.objects.filter(is_active="True")
        serializer=FacilitySerializer(facility_obj,many=True)
        return Response({'status':200,'payload':serializer.data})
    
    
    def post(self,request):
        hashes_pass=make_password(request.data['password'])
        serializer=FacilitySerializer(data=request.data)
        if((MyUser.objects.filter(email=request.data['email']))or(Facility.objects.filter(email=request.data['email']))):
                return Response({'status':403,'validationEmail':'email already exists'})
        if((MyUser.objects.filter(username=request.data['username']))or(Facility.objects.filter(username=request.data['username']))):
                return Response({'status':403,'validationUsername':'username already exists'})
                
        serializer1=MyUserSerializer(data=request.data)  
        if serializer1.is_valid(raise_exception=True):
            user=serializer1.save()
            token=get_tokens_for_user(user)
            if serializer.is_valid():
                facility=serializer.save()
                Facility.objects.filter(pk=facility.facility_id).update(user=user.id)
                MyUser.objects.filter(pk=user.id).update(user_id=facility.facility_id)
            else:   
                MyUser.objects.filter(email=user.email).delete()
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            return Response({'msg':'success','token':token})
        return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})

       

    def patch(self,request,id):
        try:
            facility_obj=Facility.objects.get(facility_id=id)
            serializer=FacilitySerializer(facility_obj,data=request.data,partial=True)
            if not serializer.is_valid():
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            if(serializer.save()):
                facility=serializer.save()
                MyUser.objects.filter(email=facility.user).update(username=facility.username,email=facility.email,position="facility")
                user=MyUser.objects.filter(email=facility.user)
                user.password=make_password("hello") 
            return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})
    
    def delete(self,request,id):
        try:
            facility_obj=Facility.objects.get(facility_id=id)
            user=MyUser.objects.get(email=facility_obj.email)
            if(facility_obj.delete()):
                if(user.delete()):
                    return Response({'status':200,'message':'deleted'})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})


#####################################################################
###### To change facility password ##################################
#####################################################################
class FacilityResetPassAPI(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        facility_obj=Facility.objects.get(facility_id=id)
        request.data['password']=base64.b64decode(request.data['password'])
        request.data['password']=request.data['password'].decode('utf-8')
        serializer=FacilitySerializer(facility_obj,data=request.data,partial=True)
        if(serializer.is_valid()):
            serializer.save()
            user=Facility.objects.filter(facility_id=id).update(password=make_password(serializer.data.get('password')))
            return Response("Password Changed")
        return Response({'message':"True"})

       

#####################################################################
###### To Display,create,update,delete clinicians ###################
#####################################################################
class ClinicianAPI(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        clinician_obj=Clinician.objects.all()
        serializer=ClinicianSerializer(clinician_obj,many=True)
        return Response({'status':200,'payload':serializer.data})
    
    def post(self,request):
        serializer=ClinicianSerializer(data=request.data)
        if((MyUser.objects.filter(email=request.data['email']))and(Clinician.objects.filter(email=request.data['email']))):
                return Response({'status':403,'validationEmail':'email already exists'})
        if((MyUser.objects.filter(username=request.data['username']))and(Clinician.objects.filter(username=request.data['username']))):
                return Response({'status':403,'validationUsername':'username already exists'})
   
        serializer1=MyUserSerializer(data=request.data)  
        if serializer1.is_valid(raise_exception=True):
            user=serializer1.save()
            token=get_tokens_for_user(user)       
            if serializer.is_valid():
                clinician=serializer.save()
                Clinician.objects.filter(pk=clinician.clinician_id).update(user=user.id)
                user=MyUser.objects.filter(pk=user.id).update(user_id=clinician.clinician_id)
            else:   
                MyUser.objects.filter(email=user.email).delete()
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            return Response({'msg':'success','token':token})
        return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
        
    def put(self,request,id):
        try:
            clinician_obj=Clinician.objects.get(clinician_id=id)
            serializer=ClinicianSerializer(clinician_obj,data=request.data)
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    clinician=serializer.save()
                    MyUser.objects.filter(email=facility.user).update(username=facility.username,email=facility.email,position="position")
                    return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})

    def patch(self,request,id):
        try:
            clinician_obj=Clinician.objects.get(clinician_id=id)
            serializer=ClinicianSerializer(clinician_obj,data=request.data,partial=True)
            def checkKey(dict,key):
                if key in dict.keys():
                    return dict[key]
                else:
                    return None
            email=checkKey(request.data,'email')
            password=checkKey(request.data,'password')
            if email is not None:
                fetched_data=Clinician.objects.get(clinician_id=id)
            
            if(serializer.is_valid(raise_exception=True)):
                
                if(serializer.save()):
                    clinician=serializer.save()
                    
                    if password is not None and fetched_data.password!=request.data['password']:
                        hashed_pass=make_password(clinician.password)
                        print("its here")
                        MyUser.objects.filter(email=clinician.user).update(username=clinician.username,email=clinician.email,password=hashed_pass)
                        Clinician.objects.filter(pk=clinician.clinician_id).update(password=hashed_pass)
                    else:
                        print("its not  here")
                        MyUser.objects.filter(email=clinician.user).update(username=clinician.username,email=clinician.email)
                    return Response({'status':200,'payload':serializer.data})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})

    def delete(self,request,id):
        try:
            clinician_obj=Clinician.objects.get(clinician_id=id)
            user=MyUser.objects.get(email=clinician_obj.email)
            if(clinician_obj.delete()):
                if(user.delete()):
                    return Response({'status':200,'message':'deleted'})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})


#####################################################################
###### To change clinician passwords ################################
#####################################################################
class ClinicianResetPassAPI(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        clinican_obj=Clinician.objects.get(clinician_id=id)
        serializer=ClinicanSerializer(clinician_obj,data=request.data,partial=True)
        if(serializer.is_valid()):
            serializer.save()
            Clinician.objects.filter(clinician_id=id).update(password=make_password(serializer.data.get('password')))
            return Response("Password Changed")
        return Response({'message':"True"})




#####################################################################
###### for changing scheduling permission ###########################
#####################################################################
class ClinicianScheduleAPI(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        try:
            clinician_obj=Clinician.objects.get(clinician_id=id)
            serializer=ClinicianSerializer(clinician_obj,data=request.data,partial=True)
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    clinician=serializer.save()
                    Clinician.objects.filter(pk=clinician.clinician_id).update(apply_shift=clinician.apply_shift,is_active=clinician.is_active)
                    return Response({'status':200,'payload':serializer.data})
           
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})


#####################################################################
###### To Display,create,update,delete admins #######################
#####################################################################
class AdminAPI(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        admin_obj=Admin.objects.all()
        serializer=AdminSerializer(admin_obj,many=True)
        return Response({'status':200,'payload':serializer.data})

    def post(self,request):
        hashed_pass=make_password(request.data['password'])
        serializer=AdminSerializer(data=request.data)

        if((MyUser.objects.filter(email=request.data['email']))or(Admin.objects.filter(email=request.data['email']))):
                return Response({'status':403,'validationEmail':'email already exists'})
        if((MyUser.objects.filter(username=request.data['username']))or(Admin.objects.filter(username=request.data['username']))):
                return Response({'status':403,'validationUsername':'username already exists'})
                
        serializer1=MyUserSerializer(data=request.data)  
        if serializer1.is_valid(raise_exception=True):
            user=serializer1.save()
            token=get_tokens_for_user(user)
            if serializer.is_valid():
                admin=serializer.save()
                Admin.objects.filter(pk=admin.admin_id).update(user=user.id,password=hashed_pass)
                MyUser.objects.filter(pk=user.id).update(Admin=admin.admin_id)
            else:   
                MyUser.objects.filter(email=user.email).delete()
                return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})
            return Response({'msg':'success','token':token})
        return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})

        
    def patch(self,request,id):
        
        try:
            admin_obj=Admin.objects.get(admin_id=id)
            serializer=AdminSerializer(admin_obj,data=request.data,partial=True)

            def checkKey(dict,key):
                if key in dict.keys():
                    return dict[key]
                else:
                    return None
            email=checkKey(request.data,'email')
            password=checkKey(request.data,'password')
            if email is not None:
                try:
                    fetched_data=Admin.objects.get(admin_id=id)
                except Exception as e:
                    print(e,"erorrrrrrrrrrrrrr")

           
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    admin_obj=serializer.save()
                   
                    if password is not None and fetched_data.password!=request.data['password']:
                        print("its here")
                        hashed_pass=make_password(admin_obj.password)
                        MyUser.objects.filter(email=admin_obj.user).update(username=admin_obj.username,email=admin_obj.email,position=admin_obj.role,password=hashed_pass)
                        Admin.objects.filter(admin_id=admin_obj.admin_id).update(email=admin_obj.email,password=hashed_pass)
                    else:
                        print("its not  here")
                        MyUser.objects.filter(email=admin_obj.user).update(username=admin_obj.username,email=admin_obj.email,position=admin_obj.role)
                    return Response({'status':200,'payload':serializer.data})
        
            
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})

    def delete(self,request,id):
        try:
            admin_obj=Admin.objects.get(admin_id=id)
            user=MyUser.objects.get(email=admin_obj.email)
            if(admin_obj.delete()):
                if(user.delete()):
                    return Response({'status':200,'message':'deleted'})
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})


#####################################################################
###### for changing scheduling permission ###########################
#####################################################################
class AdminScheduleAPI(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        try:
            admin_obj=Admin.objects.get(admin_id=id)
            serializer=AdminSerializer(admin_obj,data=request.data,partial=True)
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    admin_obj=serializer.save()
                    Admin.objects.filter(admin_id=admin_obj.admin_id).update(allow_scheduling=admin_obj.allow_scheduling,is_active=admin_obj.is_active)
                    return Response({'status':200,'payload':serializer.data})
          
        except Exception as e:
            return Response({'status':403,'message':'invalid id'})


        

#####################################################################
###### To change admin passwords ####################################
#####################################################################

class AdminResetPassAPI(APIView):
    permission_classes=[IsAuthenticated]
    def patch(self,request,id):
        admin_obj=Admin.objects.get(admin_id=id)
        request.data['password']=base64.b64decode(request.data['password'])
        request.data['password']=request.data['password'].decode('utf-8')
        serializer=AdminSerializer(admin_obj,data=request.data,partial=True) 
        if(serializer.is_valid()):
            serializer.save()
            user=Admin.objects.filter(admin_id=id).update(password=make_password(serializer.data.get('password')))
            return Response("Password Changed")
        return Response({'message':"True"})

#####################################################################
###### To display and create documents ##############################
#####################################################################
class DocumentView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=['clinician_id__clinician_id','clinician_position','clinician_name']
    filterset_fields=['clinician_id','clinician_position']
    pagination_class=DefaultCustomPagination


#####################################################################
###### To update and delete #########################################
#####################################################################
class DocumentUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    lookup_field= 'document_id'

#####################################################################
###### To display and create feedbacks ##############################
#####################################################################
class FeedbackView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["feedback","clinician_position","clinician_name"]
    filterset_fields=['feedback_id','schedule_id','clinician_id']
    pagination_class=DefaultCustomPagination

#####################################################################
###### To update and delete feedbacks ###############################
#####################################################################
class FeedbackUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    lookup_field= 'feedback_id'
 


#####################################################################
###### To display and create quotes### ##############################
#####################################################################
class Daily_QuotesView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Daily_Quotes.objects.all()
    serializer_class = Daily_QuotesSerializer
    filter_backends=[SearchFilter]
    search_fields=['daily_quotes_id']

#####################################################################
###### To update and delete quotes ##################################
#####################################################################
class Daily_QuotesUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Daily_Quotes.objects.all()
    serializer_class = Daily_QuotesSerializer
    lookup_field= 'daily_quotes_id'



#####################################################################
###### To display and create activity# ##############################
#####################################################################
class ActivityView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    filter_backends=[SearchFilter]
    search_fields=['recent_activity_id','acitivity_date','category','facility_id']

#####################################################################
###### To update and delete activity ################################
#####################################################################
class ActivityUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    lookup_field= 'recent_activity_id'

#####################################################################
###### To display and create notes ##################################
#####################################################################
class NotesView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Note.objects.all().order_by('note_id')
    serializer_class = NotesSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["note"]
    filterset_fields=['note_id','user_id']

#####################################################################
###### To update and delete notes ###################################
#####################################################################
class NotesUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Note.objects.all()
    serializer_class = NotesSerializer
    lookup_field= 'note_id'


#####################################################################
###### To display and create notifications ##########################
#####################################################################
class NotificationView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
   
    filterset_fields=['notification_id','user_id']

#####################################################################
###### To update and delete notifications ###########################
#####################################################################
class NotificationUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Notification.objects.all()
    serializer_class =  NotificationSerializer
    lookup_field= 'notification_id'

#####################################################################
###### To display and create chat ###################################
#####################################################################
class ChatView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Chat.objects.all()
    serializer_class = ChatSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    filterset_fields=['chat_id','channel']


#####################################################################
###### To update and delete chat ####################################
#####################################################################
class ChatUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset =Chat.objects.all()
    serializer_class =  ChatSerializer
    lookup_field= 'channel'

#####################################################################
###### To create,display and update chat history ####################
#####################################################################
class ChatAndHistoryAPIView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request, *args, **kwargs):
        queryset=ChatHistory.objects.all()
        # custom Filter parameters
        user_id=self.request.GET.get('user_id',None)
        if (user_id):
            queryset=queryset.filter(user_id=user_id)
        serializer=ChatHistorySerializer(queryset,many=True)
        return Response(serializer.data)

    def post(self,request):
        def checkKey(dict,key):
            if key in dict.keys():
                return dict[key]
            else:
                return None
        channel=checkKey(request.data,'channel')       
        user_id=checkKey(request.data,'user_id')
        peer_id=checkKey(request.data,'peer_id')
        user_fullname=checkKey(request.data,'user_fullname')
        peer_fullname=checkKey(request.data,'peer_fullname')
        user_avatar_image=checkKey(request.data,'user_avatar_image')
        peer_avatar_image=checkKey(request.data,'peer_avatar_image')
        last_message=checkKey(request.data,'last_message')
        message_by=checkKey(request.data,'message_by')
        message_to=checkKey(request.data,'message_to')
        message_time=checkKey(request.data,'message_time')
        displayed=checkKey(request.data,'displayed')
        serializer=ChatSerializer(data=request.data)
        serializer1=ChatHistorySerializer(data=request.data)
        serializer2=ChatHistorySerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            chat=serializer.save()
        if serializer1.is_valid():
                data=serializer1.save()
                if serializer2.is_valid():
                    data1=serializer2.save()
                    changed=ChatHistory.objects.filter(pk=data1.chat_hisory_id).update(user_id=peer_id,peer_id=user_id,user_fullname=peer_fullname,peer_fullname=user_fullname,user_avatar_image=peer_avatar_image,peer_avatar_image=user_avatar_image)
                    return Response({'status':200,'user':serializer.data,'peer':changed})
                else:
                    return Response({'status':400,'error':serializer2.errors})
        else:
            return Response({'status':400,'error':serializer1.errors})
        return Response({'status':403,'errors':serializer.errors,'message':'something went wrong'})

    def patch(self,request,id):
        def checkKey(dict,key):
            if key in dict.keys():
                return dict[key]
            else:
                return None
        channel=checkKey(request.data,'channel')       
        user_id=checkKey(request.data,'user_id')
        peer_id=checkKey(request.data,'peer_id')
        user_fullname=checkKey(request.data,'user_fullname')
        peer_fullname=checkKey(request.data,'peer_fullname')
        user_avatar_image=checkKey(request.data,'user_avatar_image')
        peer_avatar_image=checkKey(request.data,'peer_avatar_image')
        last_message=checkKey(request.data,'last_message')
        message_by=checkKey(request.data,'message_by')
        message_to=checkKey(request.data,'message_to')
        message_time=checkKey(request.data,'message_time')
        displayed=checkKey(request.data,'displayed')
        try:
            chat_obj=Chat.objects.get(channel=id)
            serializer=ChatSerializer(chat_obj,data=request.data,partial=True)
            if(serializer.is_valid(raise_exception=True)):
                if(serializer.save()):
                    chat_obj=serializer.save()
                    if None not in (displayed,channel,user_id,peer_id,user_fullname,peer_fullname,user_avatar_image,peer_avatar_image,last_message, message_by,message_to,message_time,displayed):
                        data1=ChatHistory.objects.filter(channel=channel,user_id=user_id).update(last_message=last_message,message_by=message_by,message_to=message_to,peer_fullname=peer_fullname,user_fullname=user_fullname,peer_avatar_image=peer_avatar_image,user_avatar_image=user_avatar_image,displayed=displayed,message_time=message_time)
                        data2=ChatHistory.objects.filter(channel=channel,user_id=peer_id).update(last_message=last_message,message_by=message_by,message_to=message_to,peer_fullname=user_fullname,user_fullname=peer_fullname,peer_avatar_image=user_avatar_image,user_avatar_image=peer_avatar_image,displayed=displayed,message_time=message_time)
                        return Response({'status':200,'payload':'Success'})
                    elif None not in (displayed,channel,user_id,peer_id):
                        data11=ChatHistory.objects.filter(channel=channel,user_id=user_id).update(displayed=displayed)
                        data22=ChatHistory.objects.filter(channel=channel,user_id=peer_id).update(displayed=displayed)
                        return Response({'status':200,'payload':'Success'})
                    elif None not in (peer_avatar_image,user_avatar_image,channel,user_id,peer_id):
                        data11=ChatHistory.objects.filter(channel=channel,user_id=user_id).update(displayed=displayed)
                        data22=ChatHistory.objects.filter(channel=channel,user_id=peer_id).update(displayed=displayed)
                        return Response({'status':200,'payload':'Success'})
                    else:
                        data1=ChatHistory.objects.filter(channel=channel,user_id=user_id).update(last_message=last_message,message_by=message_by,message_to=message_to,peer_fullname=peer_fullname,user_fullname=user_fullname,peer_avatar_image=peer_avatar_image,user_avatar_image=user_avatar_image)
                        data2=ChatHistory.objects.filter(channel=channel,user_id=peer_id).update(last_message=last_message,message_by=message_by,message_to=message_to,peer_fullname=user_fullname,user_fullname=peer_fullname,peer_avatar_image=user_avatar_image,user_avatar_image=peer_avatar_image)
                    #Admin.objects.filter(admin_id=admin_obj.admin_id).update(password=hashed_pass)
                        return Response({'status':200,'payload':serializer.data})
        
            
        except Exception as e:
            return Response({'status':403,'message':'Not found.'})




#####################################################################
###### To create and display token ##################################
#####################################################################
class TokenView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Token.objects.all()
    serializer_class = TokenSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    filterset_fields=['token_id','user_id']

#####################################################################
###### To update and delete chat history ############################
#####################################################################
class TokenUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset =Token.objects.all()
    serializer_class =  TokenSerializer
    lookup_field= 'user_id'


#####################################################################
###### To create and display invoice ################################
#####################################################################
class InvoiceView(generics.ListAPIView,generics.CreateAPIView):
    permission_classes=[IsAuthenticated]
    queryset = Invoice.objects.all().order_by('invoice_id')
    serializer_class = InvoiceSerializer
    filter_backends=[DjangoFilterBackend,filters.SearchFilter]
    search_fields=["invoice_id"]
    filterset_fields=['invoice_id']


#####################################################################
###### To update and delete invoice #################################
#####################################################################
class InvoiceUpdateView(generics.UpdateAPIView,generics.DestroyAPIView):
    permission_classes=[IsAuthenticated]
    queryset =Invoice.objects.all()
    serializer_class =  InvoiceSerializer
    lookup_field= 'invoice_id'


#####################################################################
###### To get all users for chat ####################################
#####################################################################
class ChatUsersView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        admin_id=self.request.GET.get('admin_id',None)
        clinician_id=self.request.GET.get('clinician_id',None)

        if admin_id is not None:
            position=Admin.objects.get(pk=admin_id).role
            facilityAdmin=Admin.objects.get(pk=admin_id).facility
        elif clinician_id is not None:
            position="clinician"
            facility=Clinician.objects.get(pk=clinician_id).job_sites
       
        admin=Admin.objects.all().values()
        clinician=Clinician.objects.all().values()
        
        if position=="clinician" and facility is not None:
            clinician_filtered=[]
            for i in facility:
                adminn=Admin.objects.filter(facility__facility_name=i).values()
                print(adminn)
                for j in clinician:
                    if i in j['job_sites']:   
                        clinician_filtered.append(j)         
            return Response({'status':200,'payload':list(chain(adminn,clinician_filtered))})
        if position=="poc_admin" or position=="facility_admin" and facilityAdmin is not None:
            clinician_filtered=[]
            adminn=Admin.objects.filter(facility__facility_name=facilityAdmin).values()
          
            for j in clinician:
                
                if str(facilityAdmin) in j['job_sites']: 
                    clinician_filtered.append(j)         
            return Response({'status':200,'payload':list(chain(adminn,clinician_filtered))})
        
        
        else:
            return Response({'status':200,'payload':list(chain(admin,clinician))})


#####################################################################
###### To generate agora tokens #####################################
#####################################################################
class TokenGenerator(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request):
        appId=os.getenv('AGORA_APPID')
        appCertificate=os.getenv('AGORA_CERTIFICATE')
        channelName=request.data['channelName']
        uid="0"
        role = 1
        expirationTimeInSeconds = 86400
        currentTimestamp = math.floor(time.time())
        privilegeExpiredTs = currentTimestamp + expirationTimeInSeconds
        token = RtcTokenBuilder.buildTokenWithUid(appId, appCertificate, channelName, uid, role, privilegeExpiredTs)
        return Response({'status':200,'payload':token})


#####################################################################
###### To display filtered data for shifts ##########################
#####################################################################
class SchedulingAPiView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request, *args, **kwargs):
        queryset=Scheduling.objects.all()
        # custom Filter parameters
        clinician_id=self.request.GET.get('clinician_id',None)
        facility_id=self.request.GET.get('facility_id',None)
        from_date=self.request.GET.get('from_date',None)
        to_date=self.request.GET.get('to_date',None)

        if from_date and to_date:
            date_format='%d-%m-%Y'
            from_date=datetime.strptime(from_date,date_format)
            to_date=datetime.strptime(to_date,date_format) #convert string into date format
            to_date=to_date+timedelta(days=1) # add extra day in date search
            queryset=queryset.filter(start_time__range=[from_date,to_date])

        if (clinician_id):
            queryset=queryset.filter(clinician_id=clinician_id)
        
        if (facility_id):
            queryset=queryset.filter(facility_id=facility_id)
            
        serializer=SchedulingSerializer(queryset,many=True)
        return Response(serializer.data)

#####################################################################
###### To display filtered openshifts ###############################
#####################################################################
class OpenShiftView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request, *args, **kwargs):
        queryset=Scheduling.objects.all()
        # custom Filter parameters
        open_shift=self.request.GET.get('open_shift',None)
        clinician_position=self.request.GET.get('clinician_position',None)
        clinician_id=self.request.GET.get('clinician_id',None)
        from_date=self.request.GET.get('from_date',None)
        to_date=self.request.GET.get('to_date',None)

        if from_date and to_date:
            date_format='%d-%m-%Y'
            from_date=datetime.strptime(from_date,date_format)
            to_date=datetime.strptime(to_date,date_format) #convert string into date format
            to_date=to_date+timedelta(days=1) # add extra day in date search
            queryset=queryset.filter(start_time__range=[from_date,to_date])

        if open_shift :
            queryset=queryset.filter(status="Open")
        
        if clinician_position :
            queryset=queryset.filter(clinician_position=clinician_position)
        if clinician_id and open_shift:
            facility=Clinician.objects.get(clinician_id=clinician_id).job_sites
            print(facility,'facility')
            queryset=queryset.filter(status="Open")
            data1=Scheduling.objects.all().filter(clinician_id=clinician_id).values()
            data2=Scheduling.objects.all().filter(status="Open").values()
            
            facility_filtered2=[]
            facility_filtered1=[]
            for i in facility:
                for j in data2:
                    if i in j['facility_name']:   
                        facility_filtered2.append(j)
            for i in facility:
                for j in data1:
                    if i in j['facility_name']:   
                        facility_filtered1.append(j)
        
       
        
            

        serializer=SchedulingSerializer(queryset,many=True)
        
        format='%Y-%m-%dT%H:%M:%S'
        date=datetime.strptime('2022-11-28T15:59:59',format)
        data=[]
        data=serializer.data
        filteredData=[]

        #new_data=serializer.data
        new_data=facility_filtered2
        flag=False
        #finding overlapping shifts ans storing in 
        for i in facility_filtered2:
            for j in facility_filtered1:
                new_data=[]
                flag=True
                date1=datetime.strptime(j['start_time'],format)
                date2=datetime.strptime(i['start_time'],format)
                date3=datetime.strptime(j['end_time'],format)
    
                if date1<=date2<=date3:
                    print(date2,"date222222222222222222")
                    filteredData.append(i)
               
                seen=set()
                for i in filteredData:
                    t=tuple(i.items())
                    if t not in seen:
                        seen.add(t)
                        new_data.append(i)
        
        final=facility_filtered2
        print(final)
        # print(new_data,"newdata")
        if new_data != []:
            final=[]
            for i in new_data:
                for j in range(len(facility_filtered2)):
                    if facility_filtered2[j]['schedule_id']==i['schedule_id']:
                        pass
                    else:
                        final.append(facility_filtered2[j])
        return Response(final)
    
#####################################################################
###### To download pdf ##############################################
#####################################################################
class pdf_dw(APIView):
    def post(self,request, format=None):
        params={
            'data_obj': request.data['dataa']
        }
        file_name, status =save_pdf(params)
        if not status:
            return Response({'status':400})
        # response=HttpResponse(pdf,)
        return Response({'status':200,'path':f'/media/{file_name}.pdf'})
      
#####################################################################
###### To save pdf on local system ##################################
#####################################################################
def save_pdf(params:dict):
    template=get_template("pdf.html")
    html=template.render(params)
    response=BytesIO()
    pdf=pisa.pisaDocument(BytesIO(html.encode('UTF-8')), response)
    file_name=uuid.uuid4()
    try:
        with open(str(settings.BASE_DIR) + f'/public/static/{file_name}.pdf', 'wb+') as output:
            pdf=pisa.pisaDocument(BytesIO(html.encode('UTF-8')), output)

    except Exception as e:
        print(e)

    if pdf.err:
        return '',False
    return file_name, True


#####################################################################
###### To display filtered timesheets ###############################
#####################################################################

class TimesheetFilterView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request, *args, **kwargs):
        queryset=Timesheet.objects.all()

        # custom Filter parameters
        clinician_id=self.request.GET.get('clinician_id',None)
        facility_id=self.request.GET.get('facility_id',None)

        from_date=self.request.GET.get('from_date',None)
        to_date=self.request.GET.get('to_date',None)

        if from_date and to_date:
            date_format='%d-%m-%Y'
            from_date=datetime.strptime(from_date,date_format)
            to_date=datetime.strptime(to_date,date_format) #convert string into date format
            to_date=to_date+timedelta(days=1) # add extra day in date search
            queryset=queryset.filter(check_in_time__range=[from_date,to_date])

        if (clinician_id):
            queryset=queryset.filter(clinician_id=clinician_id)
        
        if (facility_id):
            queryset=queryset.filter(facility_id=facility_id)
            
        serializer=TimesheetSerializer(queryset,many=True)
        return Response(serializer.data)
    


#####################################################################
###### To social login through web ##################################
#####################################################################
class SocialLoginWebView(APIView):
    def post(self,request, format=None):
            email=request.data['email']
            try:
                user=MyUser.objects.get(email=email)
            except:
                return Response({'status':401,'payload':'login failed'})
            if user is not None and user.position=='clinician':
                return Response({'status':203,'payload':'Not Authorized'})
            elif user is not None and user.position!='clinician':
                # login(request,user)
                token=get_tokens_for_user(user)
                return Response({'msg':'login success','token':token,'data':{'email':email,'admin_id':str(user.Admin),"user_id":user.user_id,'position':user.position}})
            else:
                return Response({'status':401,'payload':'login failed'})

#####################################################################
###### To social login through flutter ##############################
#####################################################################
class SocialLoginView(APIView):
    def post(self,request, format=None):
            email=request.data['email']
            try:
                user=MyUser.objects.get(email=email)
            except:
                return Response({'status':401,'payload':'login failed'})
            if user is not None:
                # login(request,user)
                token=get_tokens_for_user(user)
                return Response({'msg':'login success','token':token,'data':{'email':email,'admin_id':str(user.Admin),"user_id":user.user_id,'position':user.position}})
            else:
                return Response({'status':401,'payload':'login failed'})


#####################################################################
###### To change appleId of user ####################################
#####################################################################
class AppleIdUpdate(APIView):
    def patch(self,request,id):
        user=MyUser.objects.filter(pk=id).update(apple_id=request.data['apple_id'])
        return Response({'data':{'email':MyUser.objects.get(pk=id).email,'admin_id':str(MyUser.objects.get(pk=id).Admin),"user_id":MyUser.objects.get(pk=id).user_id,'position':MyUser.objects.get(pk=id).position,'apple_id':MyUser.objects.get(pk=id).apple_id}})


#####################################################################
###### To apple login through flutter ###############################
#####################################################################
class AppleLoginView(APIView):
    def post(self,request, format=None):
            def checkKey(dict,key):
                if key in dict.keys():
                    return dict[key]
                else:
                    return None
            email=checkKey(request.data,'email')
            apple_id=checkKey(request.data,'apple_id')
            if email is not None:
                try:
                    if apple_id is not None:
                        MyUser.objects.filter(email=email).update(apple_id=request.data['apple_id'])
                    user=MyUser.objects.get(email=email)
                except:
                    return Response({'status':401,'payload':'login failed'})
                
            elif apple_id is not None:
                try:
                    user=MyUser.objects.get(apple_id=apple_id)
                except:
                    return Response({'status':401,'payload':'login failed'})
            if user is not None:
                token=get_tokens_for_user(user)
                return Response({'msg':'login success','token':token,'data':{'email':user.email,'admin_id':str(user.Admin),"user_id":user.user_id,'position':user.position,"apple_id":user.apple_id}})
            else:
                return Response({'status':401,'payload':'login failed'})

#####################################################################
###### To upload image in s3bucket ##################################
#####################################################################
class S3UploadView(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
            data=request.data['data']
            user_id=request.data['user_id']
            
           
            decodeit = open('imgToUpload.jpeg', 'wb')
            decodeit.write(base64.b64decode((data)))
            decodeit.close()

            ACCESS_KEY_ID=os.getenv('S3_ACCESS_KEY')
            ACCESS_SECRET_KEY=os.getenv('S3_SECRET_KEY')
            BUCKET_NAME=os.getenv('IMAGES_BUCKET')

            data=open('imgToUpload.jpeg', 'rb')
            s3 = boto3.resource(
                's3',
                aws_access_key_id=ACCESS_KEY_ID,
                aws_secret_access_key=ACCESS_SECRET_KEY,
                config=Config(signature_version='s3v4')

            )
            filename= "image"+str(random.randint(0,10000000))+user_id+'.jpeg'
            s3.Bucket(BUCKET_NAME).put_object(Key=filename,Body=data,ContentType='image/jpeg')
            url='https://%s.s3.amazonaws.com/%s' % (BUCKET_NAME,filename)
    
            return Response({'status':200,'location':url})

#####################################################################
###### To upload document in s3bucket ###############################
#####################################################################
class S3UploadDocumentView(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
            data=request.data['data']
            user_id=request.data['user_id']
           
            decodeit = open('document.pdf', 'wb')
            decodeit.write(base64.b64decode((data)))
            decodeit.close()

            ACCESS_KEY_ID=os.getenv('S3_ACCESS_KEY')
            ACCESS_SECRET_KEY=os.getenv('S3_SECRET_KEY')
            BUCKET_NAME=os.getenv('DOCUMENTS_BUCKET')

            data=open('document.pdf', 'rb')
            s3 = boto3.resource(
                's3',
                aws_access_key_id=ACCESS_KEY_ID,
                aws_secret_access_key=ACCESS_SECRET_KEY,
                config=Config(signature_version='s3v4')
            )
            filename= str(random.randint(0,100000000))+user_id+'.pdf'
            s3.Bucket(BUCKET_NAME).put_object(Key=filename,Body=data,ContentType='application/pdf')
            url='https://%s.s3.amazonaws.com/%s' % (BUCKET_NAME,filename)
                
            return Response({'status':200,'location':url})


#####################################################################
###### To upload image in s3bucket for chat #########################
#####################################################################


class S3UploadFileView(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
            data=request.data['data']
            user_id=request.data['user_id']
            formatType=request.data['type']
            file_size=len(data)*3/4-data.count('=')

            def checkKey(dict,key):
                if key in dict.keys():
                    return dict[key]
                else:
                    return None
            nameCheck=checkKey(request.data,'name')

            if nameCheck is not None:
                name=request.data['name']
            else:
                name="file"+str(random.randint(0,10000000))+user_id+'.'+formatType

            if file_size>=1000000.0:
                return Response({'status':413,'error':'request is larger than the server is able to handle'},status=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE)


            decodeit = open('document.pdf', 'wb')
            decodeit.write(base64.b64decode((data)))
            decodeit.close()

            ACCESS_KEY_ID=os.getenv('S3_ACCESS_KEY')
            ACCESS_SECRET_KEY=os.getenv('S3_SECRET_KEY')
            BUCKET_NAME=os.getenv('DOCUMENTS_BUCKET')

            data=open('document.pdf', 'rb')
            s3 = boto3.resource(
                's3',
                aws_access_key_id=ACCESS_KEY_ID,
                aws_secret_access_key=ACCESS_SECRET_KEY,
                config=Config(signature_version='s3v4')
            )
            #filename= str(random.randint(0,100000000))+user_id+'.'+formatType.split("/")[1]
            filename=name
            s3.Bucket(BUCKET_NAME).put_object(Key=filename,Body=data,ContentType=formatType)
            url='https://%s.s3.amazonaws.com/%s' % (BUCKET_NAME,filename)
           
            return Response({'status':200,'location':url})


#####################################################################
###### function to display privacy page #############################
#####################################################################
def privacy(request):
    return render(request,'shiftalerts-privacy.html')


#####################################################################
###### function to display terms & conditions page ##################
#####################################################################
def termsandconditions(request):  
    return render(request,'terms&conditions.html')

#####################################################################
###### To display and create chat notification ##################
#####################################################################
class  ChatNotificationView(APIView):
    permission_classes=[IsAuthenticated]
    def get(self,request):
        userId=self.request.GET.get('userId',None)
        data=ChatNotification.objects.get(userId=userId)
        print(userId,"useriddddddddddddddddddd")
        queryset=ChatNotification.objects.all()
        queryset=queryset.filter(userId=userId)
            
        serializer=ChatNotificationSerializer(queryset,many=True)
        return Response({'platform':data.platform,'payload':serializer.data})
    def post(self,request, format=None):
            userId=request.data['userId']

            try:
                ChatNotification.objects.filter(userId=userId).update(message=request.data['message'],message_type=request.data['message_type'])
                user=MyUser.objects.get(email=email)
            except:
                return Response({'status':401,'payload':'login failed'})
            return Response({'status':401,'payload':'login failed'})


#####################################################################
###### To send flutter notification ##################
#####################################################################

class  FCMData(APIView):
     
    permission_classes=[IsAuthenticated]
    def post(self,request, format=None):
       
        token=Token.objects.get(user_id="88").token
        body=request.data['message']
       
            
        if request.data['message']=='Remote Call Accepted':
            body=request.data['message']
        elif request.data['message']=='Remote Call End':
            body=request.data['message']
        elif request.data['message']=='call':
            body=request.data['message']
        print(body)
        
        content={
            'channelKey': 'shift_alerts',
            'title': request.data['peerName'],
            'body': body,
            'largeIcon': '',
            'bigPicture': '',
            'devide_type':'web'
        }
       
        #removing item from request.data dict
        request.data.pop('receiverId') 
        # messaging.send_to_topic("topics-all", "title", "body from topic")
        messaging.send_to_token(
            token, "Received a new message", "Received a new message",request.data,content)
        return Response({'status':200,'payload':request.data})
        


#####################################################################
###### To send document upload notification ##################
#####################################################################

class NotificationUploadView(APIView):
    permission_classes=[IsAuthenticated]
    def post(self,request):

        # admin_id=self.request.GET.get('admin_id',None)
        clinician_id=request.data['clinician_id']
        position="clinician"
        facility=Clinician.objects.get(pk=clinician_id).job_sites
        admin=Admin.objects.filter(role="hmc_admin").values()
        
        clinician_name=f'{Clinician.objects.get(pk=clinician_id).firstname+" "+Clinician.objects.get(pk=clinician_id).lastname}'
  
        admin_filtered=[]
        for i in facility:
            adminn=Admin.objects.filter(facility__facility_name=i).values()
            admin_filtered.append(adminn)
        for i in adminn:
            request.data['user_id']=i['user_id']
            request.data['notification']=f'Document uploaded by {clinician_name}'
            serializer=NotificationSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
        for i in admin:
            request.data['user_id']=i['user_id']
            request.data['notification']=f'Document uploaded by {clinician_name}'
            serializer=NotificationSerializer(data=request.data)
            if serializer.is_valid(raise_exception=True):
                serializer.save()
        return Response({'status':200,'payload':"Successfully uploaded notification"})