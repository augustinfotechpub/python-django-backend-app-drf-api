from django.contrib import admin
from django.urls import path,include
from .views import *

urlpatterns = [

    
    path('user/register/',  RegisterUserView.as_view() ),

    path('user/update/',   UserPasswordChangeView1.as_view() ),

    path('user/login/',  LoginUserView.as_view() ),

    path('user/web/score/',  scoreView.as_view() ),

    path('user/web/login/',  LoginUserWebView.as_view() ),

    path('user/profile/', UserProfileView.as_view() ),

    path('user/changepassword/', UserPasswordChangeView.as_view() ),

    path('user/sendresetpwd/', SendEmailPassword.as_view() ),

    path('user/resetpwd/<uid>/<token>/', UserPasswordResetView.as_view() ),

    path('myuser/', MyUserView.as_view() ),
    path('myuser/edit/<id>/',   UserPasswordChangeView1.as_view() ),
    path('userprofile/edit/<id>/',   UserProfileUpdate.as_view() ),

    path('facility/', FacilityAPI.as_view() ),
    path('facility/edit/<facility_id>/',  FacilityUpdateView.as_view() ),
    path('facility/delete/<facility_id>/',  FacilityUpdateView.as_view() ),
    path('facility/view/', FacilityView.as_view() ),   #for searchfilters
    path('facility/resetPassword/<id>/', FacilityResetPassAPI.as_view() ),   #for password change


    path('clinician/', ClinicianAPI.as_view() ),
    path('clinician/edit/<id>/', ClinicianAPI.as_view() ),
    path('clinician/delete/<id>/', ClinicianAPI.as_view() ),
    path('clinician/view/', ClinicianView.as_view() ),    #for searchfilters
    path('clinician/resetPassword/<id>/', ClinicianResetPassAPI.as_view() ),   #for password change
    path('clinician/applyShift/<id>/', ClinicianScheduleAPI.as_view() ), #for changing apply shift and user status


    path('admin/view/', AdminView.as_view() ),
    path('admin/view/only/', OnlyAdminView.as_view() ),
    path('admin/create/',  AdminAPI.as_view()  ),
    # path('admin/delete/<admin_id>/', AdminUpdateView.as_view()  ),
    path('admin/delete/<id>/', AdminAPI.as_view() ),
    path('admin/allowScheduling/<admin_id>/',  AdminUpdateView.as_view()  ),
    path('admin/edit/<id>/',  AdminAPI.as_view()  ),
    path('admin/allowScheduling/<id>/',  AdminScheduleAPI.as_view()  ),
    path('admin/resetPassword/<id>/', AdminResetPassAPI.as_view() ),   #for password change


    path('scheduling/view/', SchedulingView.as_view() ),
    path('scheduling/create/',  SchedulingView.as_view()  ),
    path('scheduling/edit/<schedule_id>/',  SchedulingUpdateView.as_view()  ),
    path('scheduling/delete/<schedule_id>/', SchedulingUpdateView.as_view()  ),
    path('scheduling/calender/', calenderviews.as_view() ),


    path('shift/view/', Shift_View.as_view()),
    path('shift/create/', Shift_View.as_view()),
    path('shift/edit/<shift_template_id>/', Shift_Update.as_view()),
    path('shift/delete/<shift_template_id>/', Shift_Update.as_view()),
    

    path('timesheet/view/', TimesheetView.as_view() ),
    path('timesheet/create/',  TimesheetView.as_view()  ),
    path('timesheet/edit/<timesheet_id>/', TimesheetUpdateView.as_view() ),
    path('timesheet/delete/<timesheet_id>/', TimesheetUpdateView.as_view()  ),
    
  
    path('document/view/', DocumentView.as_view() ),
    path('document/create/', DocumentView.as_view()  ),
    path('document/edit/<document_id>/', DocumentUpdateView.as_view() ),
    path('document/delete/<document_id>/', DocumentUpdateView.as_view()  ),
   

    path('feedback/view/', FeedbackView.as_view() ),
    path('feedback/create/', FeedbackView.as_view()  ),
    path('feedback/edit/<feedback_id>/', FeedbackUpdateView.as_view() ),
    path('feedback/delete/<feedback_id>/', FeedbackUpdateView.as_view()  ),
   
  
    path('daily_quotes/view/', Daily_QuotesView.as_view() ),
    path('daily_quote/create/', Daily_QuotesView.as_view()  ),
    path('daily_quote/edit/<daily_quotes_id>/', Daily_QuotesUpdateView.as_view() ),
    path('daily_quote/delete/<daily_quotes_id>/', Daily_QuotesUpdateView.as_view()  ),

   
    path('activities/view/', ActivityView.as_view() ),
    path('activities/create/', ActivityView.as_view()  ),
    path('activities/edit/<recent_activity_id>/', ActivityUpdateView.as_view() ),
    path('activities/delete/<recent_activity_id>/', ActivityUpdateView.as_view()  ),

    path('notes/view/', NotesView.as_view() ),
    path('notes/create/', NotesView.as_view()  ),
    path('notes/edit/<note_id>/', NotesUpdateView.as_view() ),
    path('notes/delete/<note_id>/', NotesUpdateView.as_view()  ),

    path('notification/view/', NotificationView.as_view() ),
    path('notification/create/', NotificationView.as_view()  ),
    path('notification/edit/<notification_id>/', NotificationUpdateView.as_view() ),
    path('notification/delete/<notification_id>/', NotificationUpdateView.as_view()  ),

    path('chat/view/', ChatView.as_view() ),
    path('chat/create/', ChatView.as_view()  ),
    path('chat/edit/<channel>/', ChatUpdateView.as_view() ),
    path('chat/delete/<channel>/', ChatUpdateView.as_view()  ),


    path('chat/history/view/', ChatAndHistoryAPIView.as_view() ),
    path('chat/history/create/', ChatAndHistoryAPIView.as_view() ),
    path('chat/history/edit/<id>/', ChatAndHistoryAPIView.as_view() ),

    path('token/view/', TokenView.as_view() ),
    path('token/create/', TokenView.as_view()  ),
    path('token/edit/<user_id>/', TokenUpdateView.as_view() ),
    path('token/delete/<user_id>/', TokenUpdateView.as_view()  ),


    path('invoice/view/', InvoiceView.as_view() ),
    path('invoice/create/', InvoiceView.as_view()  ),
    path('invoice/edit/<invoice_id>/', InvoiceUpdateView.as_view() ),
    path('invoice/delete/<invoice_id>/', InvoiceUpdateView.as_view()  ),
 
    path('schedulingfilter/view/', SchedulingAPiView.as_view() ),

    path('OpenShift/view/', OpenShiftView.as_view() ),

    path('timesheetFilter/view/', TimesheetFilterView.as_view() ),
    path('pdf/', pdf_dw.as_view() ),

    #google,facebook and apple login api

    path('user/social-login/',  SocialLoginView.as_view() ),
    path('user/web/social-login/',  SocialLoginWebView.as_view() ),
    path('user/apple-login/',  AppleLoginView.as_view() ),
    path('user/apple-id/<id>/',  AppleIdUpdate.as_view()),

    # api to upload files in aws s3 bucket
    path('s3upload/image/',  S3UploadView.as_view() ),
    path('s3upload/document/',  S3UploadDocumentView.as_view() ),
    path('s3upload/chatfile/',  S3UploadFileView.as_view() ),

    path('chat/users/',  ChatUsersView.as_view() ),

    path('tokengenerator/',  TokenGenerator.as_view() ),

    path('privacy/',  privacy),

    path('termsandconditions/',  termsandconditions),

    path('chat-notification/',  ChatNotificationView.as_view()),

    path('fcm-data/',  FCMData.as_view()),

    path('document/notification/',  NotificationUploadView.as_view()),
    

]



