from django.contrib.postgres.fields import ArrayField

from wsgiref.simple_server import demo_app
from django.db import models
import sys
from datetime import date
from django.core.validators import MaxValueValidator

# Create your models here.
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, AbstractUser
)



class Role_Choices(models.TextChoices):
    hmc_admin='hmc_admin',('HMC Admin')
    facility_admin='facility_admin',('Facility Admin')
    c_level_emp='c_level_emp',('C Level Admin')
    poc_admin='poc_admin',('POC Admin')


from django.core.exceptions import ValidationError



class Role_Choices1(models.TextChoices):
    hmc_admin='hmc_admin',('HMC Admin')
    facility_admin='facility_admin',('Facility Admin')
    c_level_emp='c_level_emp',('C Level Admin')
    facility='facility',('Facility')
    clinician='clinician',('Clinician')
    poc_admin='poc_admin',('POC Admin')

class MyUserManager(BaseUserManager):
    def create_user(self, email, username, position, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
            username=username,
            position=position,
           
            password=password,
            
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, position, password=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email=email,
            username=username,
           
            position=position,
           
            password=password,
            
        )
        user.is_admin = True
        user.is_superuser=True
        user.save(using=self._db)
        return user




class MyUser(AbstractUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    username=models.CharField(max_length=50,unique=True,blank=False)
    password=models.CharField(max_length=255,blank=False)
    user_id=models.CharField(max_length=50,blank=True)
    apple_id=models.CharField(max_length=255,blank=True)
    position=models.CharField(max_length=20,choices=Role_Choices1.choices,default=Role_Choices1.hmc_admin,blank=False)
    is_active = models.BooleanField(default=True)
    Admin=models.CharField(max_length=50,blank=True)
    is_admin = models.BooleanField(default=False)
   

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username','position']

    def __str__(self):
        return self.email

    

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin



class Admin(models.Model):
    class Meta:
        db_table = "Admin"

    admin_id=models.AutoField(primary_key=True,blank=False)
    fullname=models.CharField(max_length=100,blank=False)
    firstname=models.CharField(max_length=50,blank=False)
    lastname=models.CharField(max_length=50,blank=False)
    email=models.EmailField(blank=False,unique=True)
    username=models.CharField(max_length=50,unique=True,blank=False)
    password=models.CharField(max_length=255,blank=False)
    avatar_image=models.TextField(blank=False)
    role=models.CharField(max_length=20,choices=Role_Choices.choices,default=Role_Choices.hmc_admin)
    contact_no=models.CharField(max_length=14,blank=False)
    address=models.CharField(max_length=200,blank=False)
    date_of_birth=models.DateField(help_text=('Enter the date of birth'),
        validators=[MaxValueValidator(limit_value=date.today)]
        )
    allow_scheduling=models.BooleanField(default=False)
    is_active=models.BooleanField(default=True)
    facility=models.ForeignKey('healthcare.Facility', on_delete=models.CASCADE,blank=True,null=True)
    user=models.ForeignKey(MyUser, on_delete=models.CASCADE,blank=True,null=True)

    def clean(self):
        special_symbols=['$','@','#','%']
        if len(self.password)<8:
            raise ValidationError('Password must have atleast 8 characters')
        if not any(characters.isdigit() for characters in self.password):
            raise ValidationError('Password must have atleast one numeric character')
        if not any(characters.isupper() for characters in self.password):
            raise ValidationError('Password must have atleast one uppercase character')
        if not any(characters.islower() for characters in self.password):
            raise ValidationError('Password must have atleast one lowercase character')
        if not any(characters in special_symbols for characters in self.password):
            raise ValidationError('Password must have atleast one of the symbols $@#%')


    def __str__(self):
        return str(self.admin_id)



class Facility(models.Model):
    class Meta:
        db_table = "Facility"


    facility_id=models.AutoField(primary_key=True,blank=False)
    facility_image=models.TextField(blank=False)
    address=models.CharField(max_length=200,blank=False)
    phone_no=models.CharField(max_length=14,blank=False)
    facility_name=models.CharField(max_length=50,blank=False)
    facility_geolocation=models.CharField(max_length=50,blank=True) 
    is_active = models.BooleanField(default=True)
    latitude=models.CharField(max_length=200,blank=True)
    longitude=models.CharField(max_length=200,blank=True)
    

    
    def __str__(self):
        return self.facility_name



class Position_choices(models.TextChoices):		
    cna='CNA',('CNA')		
    stna='STNA',('STNA')		
    rn='RN',('RN')		
    lpn='LPN',('LPN')		
    cma='CMA',('CMA')		
    lvn='LVN',('LVN')

class Clinician(models.Model):
    class Meta:
        db_table = "Clinician"


    clinician_id=models.AutoField(primary_key=True,blank=False)
    username=models.CharField(max_length=50,unique=True,blank=False)
    password=models.CharField(max_length=255,blank=False)
    email=models.EmailField(blank=False,unique=True)
    firstname=models.CharField(max_length=50,blank=False)
    lastname=models.CharField(max_length=50,blank=False)
    address=models.CharField(max_length=200,blank=False)
    avatar_image=models.TextField(blank=False)
    contact_no=models.CharField(max_length=14,blank=False)
    date_of_birth=models.DateField(help_text=('Enter the date of birth'),
        validators=[MaxValueValidator(limit_value=date.today)]
        )
    job_sites=ArrayField(models.CharField(max_length=255))
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    is_active = models.BooleanField(default=True)
    apply_shift=models.BooleanField(default=False)
    role=models.CharField(max_length=50,blank=False)
    user=models.ForeignKey(MyUser, on_delete=models.CASCADE,blank=True,null=True)

    def clean(self):
        special_symbols=['$','@','#','%']
        if len(self.password)<8:
            raise ValidationError('Password must have atleast 8 characters')
        if not any(characters.isdigit() for characters in self.password):
            raise ValidationError('Password must have atleast one numeric character')
        if not any(characters.isupper() for characters in self.password):
            raise ValidationError('Password must have atleast one uppercase character')
        if not any(characters.islower() for characters in self.password):
            raise ValidationError('Password must have atleast one lowercase character')
        if not any(characters in special_symbols for characters in self.password):
            raise ValidationError('Password must have atleast one of the symbols $@#%')
        
    
    def __str__(self):
        return str(self.clinician_id)




class Document(models.Model):
    class Meta:
        db_table = "Documents"

    document_id=models.AutoField(primary_key=True,blank=False)
    clinician_id=models.ForeignKey(Clinician,on_delete=models.CASCADE)
    clinician_name=models.CharField(max_length=50,blank=False)
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    clinician_avatar=models.TextField(blank=False)
    document_name=models.CharField(max_length=50,blank=False)
    document_path=models.TextField(blank=False)
    document_updated_date=models.CharField(max_length=50,blank=False)
    document_status=models.CharField(max_length=50,blank=False)
    facility=ArrayField(models.CharField(max_length=255),blank=True,null=True)

    
    def __str__(self):
        return self.document_name

    
    



class Feedback(models.Model):
    class Meta:
        db_table = "Feedback"

    feedback_id=models.AutoField(primary_key=True,blank=False)
    schedule_id=models.CharField(max_length=50,blank=True)
    clinician_id=models.ForeignKey(Clinician,on_delete=models.DO_NOTHING)
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    clinician_name=models.CharField(max_length=50,blank=False)
    ratings=models.IntegerField(max_length=5,blank=False)
    feedback=models.CharField(max_length=200,blank=False)
    status=models.CharField(max_length=50,blank=False)

    
    def __str__(self):
        return self.clinician_position


class Shift_template(models.Model):
    class Meta:
        db_table = "Shifts"

    shift_template_id=models.AutoField(primary_key=True,blank=False)
    template_name=models.CharField(max_length=50,blank=False)
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    shift_title=models.CharField(max_length=50,blank=False)
    shift_color=models.CharField(max_length=50,blank=False) 
    unit=models.CharField(max_length=50,blank=True)      
    start_date=models.DateField(blank=True)
    start_time=models.CharField(max_length=50,blank=False)
    end_date=models.DateField(blank=True)
    end_time=models.CharField(max_length=50,blank=False)
    daily_repeat = models.BooleanField(default=True)
    shift_note=models.TextField(blank=True)

    
    def __str__(self):
        return self.clinician_position


class Timesheet(models.Model):
    class Meta:
        db_table = "Timesheet"

    timesheet_id=models.AutoField(primary_key=True,blank=False)
    shift_id=models.CharField(max_length=50,blank=False)
    clinician_id=models.CharField(max_length=50,blank=False)
    clinician_name=models.CharField(max_length=50,blank=False)
    feedback_id=models.CharField(max_length=50,blank=True)
    facility_name=models.CharField(max_length=100,blank=False) # changes
    facility_id=models.CharField(max_length=50,blank=True)
    poc_admin=models.CharField(max_length=100,blank=False) # changes
    shift_color=models.CharField(max_length=50,blank=False) #changes
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    break_in_date=models.CharField(max_length=50,blank=True)
    check_in_time=models.CharField(max_length=50,blank=False)
    break_out_date=models.CharField(max_length=50,blank=True)
    check_out_time=models.CharField(max_length=50,blank=False)
    shift_date=models.CharField(max_length=50,blank=False)
    clinician_note=models.TextField(blank=True)
    facility_note=models.TextField(blank=True)
    status=models.CharField(max_length=50,blank=False,default="Pending")
    ratings=models.CharField(max_length=50,blank=True)
    feedback=models.CharField(max_length=50,blank=True)
    poc_signature=models.CharField(max_length=50,blank=False,default="Pending")


    def __str__(self):
        return self.clinician_position


class Scheduling(models.Model):
    class Meta:
        db_table = "Scheduling"

    schedule_id=models.AutoField(primary_key=True,blank=False)
    title=models.CharField(max_length=50,blank=False)
    facility_id=models.CharField(max_length=50,blank=True)
    facility_name=models.CharField(max_length=100,blank=True) # changes
    clinician_id=models.CharField(max_length=50,blank=True)
    avatar_image=models.CharField(max_length=255,blank=True)
    shift_template_id=models.CharField(max_length=50,blank=True)
    clinician_name=models.CharField(max_length=50,blank=True)
    shift_color=models.CharField(max_length=50,blank=False) 
    clinician_position=models.CharField(max_length=50,choices=Position_choices.choices,default=Position_choices.cna,blank=False)
    start_date=models.DateField(blank=False)
    start_time=models.CharField(max_length=50,blank=True)
    end_date=models.DateField(blank=False)
    end_time=models.CharField(max_length=50,blank=False)
    status=models.CharField(max_length=50,blank=False,default="Pending")
    declined_by=models.CharField(max_length=50,blank=False,default="None")
    note=models.TextField(max_length=200,blank=True)
    timesheet_id=models.CharField(max_length=50,blank=True)
    check_in=models.CharField(max_length=50,blank=True)
    check_out=models.CharField(max_length=50,blank=True)
    break_in=models.CharField(max_length=50,blank=True)
    break_out=models.CharField(max_length=50,blank=True)
    unit=models.CharField(max_length=50,blank=False,default="default")
    no_meal_break=models.CharField(max_length=50,blank=True)
    requesting_users=ArrayField(models.CharField(max_length=255),blank=True,null=True)
    def __str__(self):
        return self.clinician_position




class Daily_Quotes(models.Model):
    class Meta:
        db_table = "Daily_Quotes"
        verbose_name="Daily_Quote"

    daily_quotes_id=models.AutoField(primary_key=True,blank=False)
    quote=models.CharField(max_length=255,blank=False)
   
    

    def __str__(self):
        return self.quote


class Activity_choices(models.TextChoices):
    success='success',('SUCCESS')
    failed='failed',('FAILED')
    processing='processing',('PROCESSING')
    pending='pending',('PENDING')

class Activity(models.Model):
    class Meta:
        db_table = "Recent_Activity"

    recent_activity_id=models.AutoField(primary_key=True,blank=False)
    facility_id=models.CharField(max_length=255,blank=False,default=None)
    activity_title=models.CharField(max_length=255,blank=False)
    activity_status=models.CharField(max_length=50,choices=Activity_choices.choices,default=Activity_choices.success,blank=False)
    acitivity_date=models.DateField(blank=False)
    category=models.CharField(max_length=255,blank=False,default=None)

    def __str__(self):
        return self.activity_title


class Note(models.Model):
    class Meta:
        db_table = "Note"

    note_id=models.AutoField(primary_key=True,blank=False)
    user_id=models.CharField(max_length=50,blank=False)
    note=models.TextField()
    attribute=models.CharField(max_length=255,blank=True,null=True)
    created_date=models.DateField(blank=False)
    created_time=models.CharField(max_length=50,blank=False)
    
    def __str__(self):
        return str(self.note_id)

class Notification(models.Model):
    class Meta:
        db_table = "Notification"

    notification_id=models.AutoField(primary_key=True,blank=False)
    user_id=models.CharField(max_length=50,blank=False)
    notification=models.TextField()
    created_time=models.CharField(max_length=50,blank=False)
    seen=models.CharField(max_length=50,default="active")
    
    def __str__(self):
        return self.notification

class Chat(models.Model):
    class Meta:
        db_table = "Chat"

    chat_id=models.AutoField(primary_key=True,blank=False)
    channel=models.CharField(max_length=255,unique=True)
    chat_data=models.TextField()
    last_chat=models.CharField(max_length=255,blank=True)
    last_chat_by=models.CharField(max_length=255,blank=True)
    read=models.CharField(max_length=255,default='true')

    
    def __str__(self):
        return str(self.chat_id)

class ChatHistory(models.Model):
    class Meta:
        db_table = "ChatHistory"

    chat_hisory_id=models.AutoField(primary_key=True,blank=False)
    channel=models.CharField(max_length=255)
    user_id=models.CharField(max_length=255)
    peer_id=models.CharField(max_length=255)
    user_fullname=models.CharField(max_length=255)
    peer_fullname=models.CharField(max_length=255)
    user_avatar_image=models.CharField(max_length=255)
    peer_avatar_image=models.CharField(max_length=255)
    last_message=models.CharField(max_length=255)
    message_by=models.CharField(max_length=255,blank=True)
    message_to=models.CharField(max_length=255,blank=True)
    message_time=models.CharField(max_length=255,blank=True)
    displayed=models.CharField(max_length=255,default='false')

class Token(models.Model):
    class Meta:
        db_table = "Token"

    token_id=models.AutoField(primary_key=True,blank=False)
    user_id=models.CharField(max_length=255)
    token=models.TextField(blank=True)

    def __str__(self):
        return str(self.user_id)

class Invoice(models.Model):
    class Meta:
        db_table = "Invoice"

    invoice_id=models.AutoField(primary_key=True,blank=False)
    service_date=models.CharField(max_length=255)
    clinician_name=models.CharField(max_length=255)
    position=models.CharField(max_length=255)
    hrs_worked=models.CharField(max_length=255)
    rate=models.CharField(max_length=255)
    amount=models.CharField(max_length=255)

    def __str__(self):
        return str(self.invoice_id)

class ChatNotification(models.Model):
    class Meta:
        db_table = "ChatNotification"

    chat_notification_id=models.AutoField(primary_key=True,blank=False)
    channelId=models.CharField(max_length=255)
    platform=models.CharField(max_length=255)
    userId=models.CharField(max_length=255)
    peerId=models.CharField(max_length=255)
    peerName=models.CharField(max_length=255)
    message=models.CharField(max_length=255)
    message_type=models.CharField(max_length=255)
    image=models.CharField(max_length=255)

    def __str__(self):
        return str(self.chat_notification_id)




