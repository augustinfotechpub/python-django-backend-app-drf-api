import email
from turtle import position
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Register your models here.
from.models import MyUser,Facility, Clinician,ChatNotification, Admin, Document, ChatHistory,Invoice, Scheduling, Feedback, Shift_template,Chat,Timesheet,Activity, Daily_Quotes, Note, Notification, Token

# Register your models here.
class UserModelAdmin(BaseUserAdmin):
   

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id','email','username', 'is_admin',)
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('username','position','Admin','user_id')}),
        ('Permissions', {'fields': ('is_admin','groups','user_permissions','is_superuser','is_active')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email','username','position','password1','password2'),
        }),
    )

    search_fields = ('email',)
    ordering = ('email',)
    # filter_horizontal = ()
    

# Now register the new UserAdmin...
admin.site.register(MyUser, UserModelAdmin)

class UserdetailsInline(admin.StackedInline):
    model=MyUser
    extra=1
    #exclude=('user_id',)
    # list_display = ('id','email','username', 'is_admin')
    list_filter = ('is_admin',)
    
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()
    

class AdminMasterr(admin.ModelAdmin):
    inlines=[UserdetailsInline]

#admin.site.register(Admin,AdminMasterr)
admin.site.register(Document)
admin.site.register(Scheduling)
admin.site.register(Feedback)
admin.site.register(Shift_template)
admin.site.register(Timesheet)



from django import forms
from django.core.exceptions import ValidationError
class StateAdmin(admin.ModelAdmin):
    list_display = ('admin_id','email','username', 'role','is_active')
    def save_model(self, request, obj, form, change):
        email1=obj.email
        if(Admin.objects.filter(admin_id=obj.admin_id)):
            from django.contrib.auth.hashers import make_password
            Admin.objects.filter(admin_id=obj.admin_id).update(fullname=obj.fullname,email=obj.email,username=obj.username,password=obj.password,avatar_image=obj.avatar_image,role=obj.role,contact_no=obj.contact_no,address=obj.address,date_of_birth=obj.date_of_birth,allow_scheduling=obj.allow_scheduling,facility=obj.facility,is_active=obj.is_active,user=obj.user)
            # print(user.email,"usersssssssssssssss")
            user=MyUser.objects.get(Admin=obj.admin_id)
            user.set_password(obj.password)
            user.save()
           # print(user.password.set_password("kuldeep"),"hashed password")
            print(user.password)
            if(obj.role=='c_level_emp'):
                MyUser.objects.filter(email=obj.email).update(Admin=obj.admin_id,is_superuser=True)
            MyUser.objects.filter(Admin=obj.admin_id).update(Admin=obj.admin_id,username=obj.username,email=obj.email,position=obj.role)
        else:
            
            # if MyUser.objects.filter(email=obj.email).count()>0:
            #     raise forms.ValidationError('Email Already Exists')
            MyUser.objects.create_user(username=obj.username,email=obj.email,position=obj.role,password=obj.password)
            
            super().save_model(request,obj,form,change)
            user=Admin.objects.get(email=email1)
            from django.contrib.auth.models import Group
            g=Group.objects.get(name='hmc_admin')
            if(g):
                if(obj.role=='hmc_admin'):
                    userr=MyUser.objects.get(email=user.email)
                    g.user_set.add(userr.id)
            if(obj.role=='c_level_emp'):
                MyUser.objects.filter(email=user.email).update(Admin=user.admin_id,is_superuser=True)
            MyUser.objects.filter(email=user.email).update(Admin=user.admin_id,is_admin=True)
            from django.contrib.auth.hashers import make_password
            Admin.objects.filter(admin_id=obj.admin_id).update(password=obj.password)



# class StateFacility(admin.ModelAdmin):
#     list_display = ('facility_name','poc_firstname','poc_lastname','phone_no','email','is_active')
#     def save_model(self, request, obj, form, change):
#         email1=obj.email
#         if(Facility.objects.filter(facility_id=obj.facility_id)):
#             from django.contrib.auth.hashers import make_password
#             Facility.objects.filter(facility_id=obj.facility_id).update(username=obj.username,email=obj.email,password=make_password(obj.password),facilty_image=obj.facilty_image,poc_position=obj.poc_position,phone_no=obj.phone_no,address=obj.address,date_of_birth=obj.date_of_birth,allow_scheduling=obj.allow_scheduling,facility_name=obj.facility_name,facility_geolocation=obj.facility_geolocation,is_active=obj.is_active,timesheet_approval=obj.timesheet_approval,geo_fencing=obj.geo_fencing,user=obj.user,poc_firstname=obj.poc_firstname,poc_lastname=obj.poc_lastname)
#             # print(user.email,"usersssssssssssssss")
#             MyUser.objects.filter(user_id=obj.facility_id,position='facility').update(user_id=obj.facility_id,username=obj.username,email=obj.email,password=obj.password,position="facility")

           
#         else:
            
#             MyUser.objects.create_user(username=obj.username,email=obj.email,position="facility",password=obj.password)
            
#             super().save_model(request,obj,form,change)
#             user=Facility.objects.get(email=email1)
#             print(user.email,"usersssssssssssssss")
#             MyUser.objects.filter(email=user.email).update(user_id=user.facility_id)
#             user_id=MyUser.objects.get(email=obj.email)
#             Facility.objects.filter(email=obj.email).update(user=user_id.id)
        




class StateClinician(admin.ModelAdmin):
    list_display = ('firstname','lastname','contact_no','clinician_position','email','is_active')
    def save_model(self, request, obj, form, change):
        email1=obj.email
        if(Clinician.objects.filter(clinician_id=obj.clinician_id)):
            from django.contrib.auth.hashers import make_password
            Clinician.objects.filter(clinician_id=obj.clinician_id).update(username=obj.username,email=obj.email,password=obj.password,avatar_image=obj.avatar_image,role=obj.role,contact_no=obj.contact_no,date_of_birth=obj.date_of_birth,is_active=obj.is_active,user=obj.user,firstname=obj.firstname,lastname=obj.lastname,apply_shift=obj.apply_shift,clinician_position=obj.clinician_position,job_sites=obj.job_sites,address=obj.address)
            # print(MyUser.objects.filter(user_id=obj.clinician_id,position='clinician'),"usersssssssssssssss")
            MyUser.objects.filter(user_id=obj.clinician_id,position='clinician').update(user_id=obj.clinician_id,username=obj.username,email=obj.email,password=make_password(obj.password),position="clinician")
        else:

            MyUser.objects.create_user(username=obj.username,email=obj.email,position="clinician",password=obj.password)
            
            super().save_model(request,obj,form,change)
            user=Clinician.objects.get(email=email1)
            print(user.email,"usersssssssssssssss")
            MyUser.objects.filter(email=user.email).update(user_id=user.clinician_id)
            user_id=MyUser.objects.get(email=obj.email)
            Clinician.objects.filter(email=obj.email).update(user=user_id.id)
            
    


  
admin.site.register(Admin, StateAdmin)
# admin.site.register(Facility, StateFacility)
admin.site.register(Facility)
admin.site.register(Clinician, StateClinician)
admin.site.register(Daily_Quotes)

admin.site.register(Activity)
admin.site.register(Note)
admin.site.register(Notification)
admin.site.register(Chat)
admin.site.register(Token)
admin.site.register(Invoice)
admin.site.register(ChatHistory)
admin.site.register(ChatNotification)



admin.site.index_title = "Welcome to Healthshiftalerts"	
admin.site.site_title = "Healthshiftalerts"	
admin.site.site_header = "Healthshiftalerts"