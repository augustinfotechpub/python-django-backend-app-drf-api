# Generated by Django 4.0.5 on 2022-08-24 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0016_shift_template_daily_repeat_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='scheduling',
            name='is_allocated',
        ),
        migrations.AddField(
            model_name='scheduling',
            name='status',
            field=models.CharField(default='Pending', max_length=50),
        ),
    ]
