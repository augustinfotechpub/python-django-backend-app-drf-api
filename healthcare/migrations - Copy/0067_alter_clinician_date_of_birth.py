# Generated by Django 4.0.5 on 2022-10-05 04:37

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0066_alter_clinician_job_sites'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clinician',
            name='date_of_birth',
            field=models.DateField(help_text='Enter the date of birth', validators=[django.core.validators.MaxValueValidator(limit_value=datetime.date.today)]),
        ),
    ]
