# Generated by Django 4.0.5 on 2022-10-05 10:36

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0072_alter_note_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='note',
            name='attribute',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=255), blank=True, null=True, size=None),
        ),
    ]
