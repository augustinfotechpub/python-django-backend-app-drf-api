# Generated by Django 4.0.5 on 2022-12-09 10:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0110_chatnotification_channelid'),
    ]

    operations = [
        migrations.AddField(
            model_name='chatnotification',
            name='image',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='message',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='message_type',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='peerId',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='peerName',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='platform',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='chatnotification',
            name='userId',
            field=models.CharField(default='1', max_length=255),
            preserve_default=False,
        ),
    ]
