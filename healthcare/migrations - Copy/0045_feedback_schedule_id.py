# Generated by Django 4.0.5 on 2022-09-14 06:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0044_timesheet_status_alter_timesheet_poc_signature'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='schedule_id',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
