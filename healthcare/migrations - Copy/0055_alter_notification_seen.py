# Generated by Django 4.0.5 on 2022-09-22 05:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0054_alter_notification_seen'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='seen',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
