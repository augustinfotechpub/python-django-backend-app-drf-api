# Generated by Django 4.0.5 on 2022-10-28 05:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0091_chat_last_chat_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chat',
            name='channel',
            field=models.CharField(max_length=255, unique=True),
        ),
    ]
