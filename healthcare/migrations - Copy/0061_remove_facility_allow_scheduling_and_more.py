# Generated by Django 4.0.5 on 2022-09-27 05:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0060_admin_user_alter_myuser_admin_alter_myuser_user_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='facility',
            name='allow_scheduling',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='date_of_birth',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='email',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='fullname',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='password',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='poc_firstname',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='poc_lastname',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='poc_position',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='timesheet_approval',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='user',
        ),
        migrations.RemoveField(
            model_name='facility',
            name='username',
        ),
        migrations.AlterField(
            model_name='admin',
            name='role',
            field=models.CharField(choices=[('hmc_admin', 'HMC Admin'), ('facility_admin', 'Facility Admin'), ('c_level_emp', 'C Level Admin'), ('poc_admin', 'POC Admin')], default='hmc_admin', max_length=20),
        ),
        migrations.AlterField(
            model_name='myuser',
            name='position',
            field=models.CharField(choices=[('hmc_admin', 'HMC Admin'), ('facility_admin', 'Facility Admin'), ('c_level_emp', 'C Level Admin'), ('facility', 'Facility'), ('clinician', 'Clinician'), ('poc_admin', 'POC Admin')], default='hmc_admin', max_length=20),
        ),
    ]
