# Generated by Django 4.0.5 on 2022-11-16 05:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('healthcare', '0103_delete_chathistory'),
    ]

    operations = [
        migrations.CreateModel(
            name='ChatHistory',
            fields=[
                ('chat_hisory_id', models.AutoField(primary_key=True, serialize=False)),
                ('channel', models.CharField(max_length=255)),
                ('user_id', models.CharField(max_length=255)),
                ('peer_id', models.CharField(max_length=255)),
                ('user_username', models.CharField(max_length=255)),
                ('peer_username', models.CharField(max_length=255)),
                ('user_avatar_image', models.CharField(max_length=255)),
                ('peer_avatar_image', models.CharField(max_length=255)),
                ('last_message', models.CharField(max_length=255)),
                ('message_by', models.CharField(blank=True, max_length=255)),
                ('message_to', models.CharField(blank=True, max_length=255)),
                ('message_time', models.CharField(blank=True, max_length=255)),
                ('displayed', models.CharField(default='false', max_length=255)),
            ],
            options={
                'db_table': 'ChatHistory',
            },
        ),
    ]
